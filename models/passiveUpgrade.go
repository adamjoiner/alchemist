package models

import (
	"strings"

	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
)

//PassiveUpgrade struct
type PassiveUpgrade struct {
	//Localization
	PassiveUpgradeDisplayName map[string]string `json:"name"`
	PassiveUpgradeDescription map[string]string `json:"description"`

	ResourceID             string `json:"-"`
	UnlockID               string `json:"-"`
	PassiveUpgradeCategory string `json:"-"`
	PassiveIconIdentifier  string `json:"-"`
	PassiveIndex           string `json:"-"`
	UpgradeTier            string `json:"-"`
	HeroName               string `json:"-"`
	MinHeroLevel           string `json:"-"`
}

//NewPassiveUpgrade creates new PassiveUpgrade
func NewPassiveUpgrade(iniData machine.IniData, config config.Config) (passiveUpgrade PassiveUpgrade) {
	passiveUpgrade.ResourceID = iniData.Data["ResourceID"]
	passiveUpgrade.UnlockID = iniData.Data["UnlockID"]
	passiveUpgrade.PassiveUpgradeCategory = iniData.Data["PassiveUpgradeCategory"]
	passiveUpgrade.PassiveIconIdentifier = iniData.Data["PassiveIconIdentifier"]
	passiveUpgrade.PassiveIndex = iniData.Data["PassiveIndex"]
	passiveUpgrade.UpgradeTier = iniData.Data["UpgradeTier"]
	passiveUpgrade.HeroName = iniData.Data["HeroName"]
	passiveUpgrade.MinHeroLevel = iniData.Data["MinHeroLevel"]

	//Making the maps for the localizations
	passiveUpgrade.PassiveUpgradeDisplayName = make(map[string]string)
	passiveUpgrade.PassiveUpgradeDescription = make(map[string]string)

	return passiveUpgrade
}

func (passiveUpgrade *PassiveUpgrade) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	passiveUpgrade.PassiveUpgradeDisplayName[language] = iniData.Data["PassiveUpgradeDisplayName"]
	passiveUpgrade.PassiveUpgradeDescription[language] = iniData.Data["PassiveUpgradeDescription"]

	passiveUpgrade.PassiveUpgradeDescription[language] = strings.Replace(passiveUpgrade.PassiveUpgradeDescription[language], "`skill1button", config.ConfigChangeSkillButton("`skill1button"), -1)
	passiveUpgrade.PassiveUpgradeDescription[language] = strings.Replace(passiveUpgrade.PassiveUpgradeDescription[language], "`skill2button", config.ConfigChangeSkillButton("`skill2button"), -1)
	passiveUpgrade.PassiveUpgradeDescription[language] = strings.Replace(passiveUpgrade.PassiveUpgradeDescription[language], "`skill3button", config.ConfigChangeSkillButton("`skill3button"), -1)
	passiveUpgrade.PassiveUpgradeDescription[language] = strings.Replace(passiveUpgrade.PassiveUpgradeDescription[language], "`skill4button", config.ConfigChangeSkillButton("`skill4button"), -1)
	passiveUpgrade.PassiveUpgradeDescription[language] = strings.Replace(passiveUpgrade.PassiveUpgradeDescription[language], "`focusskillbutton", config.ConfigChangeSkillButton("`focusskillbutton"), -1)
}
